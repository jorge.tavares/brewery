FROM library/openjdk:8-slim
MAINTAINER Jorge Tavares - jorge.tavares.inatel@gmail.com
COPY target/brewery-1.0.jar app.jar
ENTRYPOINT exec java -Xms724m -Xmx724m -jar /app.jar