package br.com.jorge.brewery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@SpringBootApplication
public class BreweryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BreweryApplication.class, args);
	}
}
