package br.com.jorge.brewery.beer;

import br.com.jorge.brewery.core.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */

@Entity
@Getter
@Builder
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties({"createdAt", "lastModified"})
public class Beer extends AbstractEntity {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @Column(nullable = false)
    private String style;

    @NotNull
    @Column(nullable = false)
    private Integer minTemperature;

    @NotNull
    @Column(nullable = false)
    private Integer maxTemperature;

    @NotNull
    @JsonIgnore
    @ApiModelProperty(readOnly = true)
    @Column(nullable = false)
    private Integer avgTemperature;

    Beer avgTemperature(Integer avgTemperature) {
        this.avgTemperature = avgTemperature;
        return this;
    }

}
