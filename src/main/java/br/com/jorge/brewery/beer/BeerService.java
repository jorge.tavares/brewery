package br.com.jorge.brewery.beer;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public interface BeerService {
    Beer create(Beer beer);
    Beer update(Integer id, Beer beer);
    Beer findOne(Specification<Beer> specification);
    Page<Beer> findAll(Pageable pageable);
    void delete(Integer id);
    Beer findIdeal(Integer temperature);
}
