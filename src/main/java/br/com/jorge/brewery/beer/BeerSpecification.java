package br.com.jorge.brewery.beer;

import org.springframework.data.jpa.domain.Specification;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BeerSpecification {

    public static Specification<Beer> id(Integer id) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Beer_.id), id);
    }
}
