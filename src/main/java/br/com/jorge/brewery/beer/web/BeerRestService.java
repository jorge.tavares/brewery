package br.com.jorge.brewery.beer.web;

import br.com.jorge.brewery.core.SwaggerConstant;
import br.com.jorge.brewery.beer.Beer;
import br.com.jorge.brewery.beer.BeerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static br.com.jorge.brewery.beer.BeerSpecification.id;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@RestController
@AllArgsConstructor
@RequestMapping("/v1/beers")
class BeerRestService {

    private final BeerService beerService;

    @PostMapping
    @ApiOperation(nickname = "Create Beer", value = "Create a Beer", response = Beer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = SwaggerConstant.HTTP_201_MSG),
            @ApiResponse(code = 409, message = SwaggerConstant.HTTP_409_MSG),
            @ApiResponse(code = 500, message = SwaggerConstant.HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.CREATED)
    Beer create(@RequestBody Beer beer) {
        return beerService.create(beer);
    }

    @PatchMapping("/{id}")
    @ApiOperation(nickname = "Update Beer", value = "Update a existent beer", response = Beer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstant.HTTP_200_MSG),
            @ApiResponse(code = 202, message = SwaggerConstant.HTTP_202_MSG),
            @ApiResponse(code = 400, message = SwaggerConstant.HTTP_400_MSG),
            @ApiResponse(code = 404, message = SwaggerConstant.HTTP_404_MSG),
            @ApiResponse(code = 500, message = SwaggerConstant.HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.OK)
    Beer update(@PathVariable Integer id, @RequestBody Beer beer) {
        return beerService.update(id, beer);
    }

    @GetMapping("/{id}")
    @ApiOperation(nickname = "Find One", value = "Find a Beer by ID", response = Beer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstant.HTTP_200_MSG),
            @ApiResponse(code = 202, message = SwaggerConstant.HTTP_202_MSG),
            @ApiResponse(code = 400, message = SwaggerConstant.HTTP_400_MSG),
            @ApiResponse(code = 404, message = SwaggerConstant.HTTP_404_MSG),
            @ApiResponse(code = 500, message = SwaggerConstant.HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.OK)
    Beer findOne(@PathVariable Integer id) {
        return beerService.findOne(id(id));
    }

    @GetMapping
    @ApiOperation(nickname = "Find Beers page", value = "Find all Beers pageable", response = Beer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstant.HTTP_200_MSG),
            @ApiResponse(code = 202, message = SwaggerConstant.HTTP_202_MSG),
            @ApiResponse(code = 400, message = SwaggerConstant.HTTP_400_MSG),
            @ApiResponse(code = 404, message = SwaggerConstant.HTTP_404_MSG),
            @ApiResponse(code = 500, message = SwaggerConstant.HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.OK)
    Page<Beer> findAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                        @RequestParam(defaultValue = "20", required = false) Integer size) {
        return beerService.findAll(new PageRequest(page, size));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(nickname = "Delete Beer", value = "Delete a existent Beer", response = Beer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = SwaggerConstant.HTTP_200_MSG),
            @ApiResponse(code = 202, message = SwaggerConstant.HTTP_202_MSG),
            @ApiResponse(code = 400, message = SwaggerConstant.HTTP_400_MSG),
            @ApiResponse(code = 404, message = SwaggerConstant.HTTP_404_MSG),
            @ApiResponse(code = 500, message = SwaggerConstant.HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.OK)
    void delete(@PathVariable Integer id) {
        beerService.delete(id);
    }
}
