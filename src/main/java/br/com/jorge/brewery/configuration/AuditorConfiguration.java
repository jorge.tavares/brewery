package br.com.jorge.brewery.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Configuration
@EnableJpaAuditing
public class AuditorConfiguration {

}