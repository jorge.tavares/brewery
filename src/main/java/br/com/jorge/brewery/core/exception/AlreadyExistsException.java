package br.com.jorge.brewery.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@ResponseStatus(value = HttpStatus.CONFLICT)
public class AlreadyExistsException extends RuntimeException {

    public AlreadyExistsException() {
        super();
    }
    public AlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
    public AlreadyExistsException(String message) {
        super(message);
    }
    public AlreadyExistsException(Throwable cause) {
        super(cause);
    }
}
