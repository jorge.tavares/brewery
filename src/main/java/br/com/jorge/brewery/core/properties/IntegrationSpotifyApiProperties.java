package br.com.jorge.brewery.core.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Data
@Component
@ConfigurationProperties(prefix = "integration.spotify.api")
public class IntegrationSpotifyApiProperties {
    private String clientId;
    private String clientSecret;
}
