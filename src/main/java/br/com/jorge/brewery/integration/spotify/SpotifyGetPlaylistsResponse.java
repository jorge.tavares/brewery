package br.com.jorge.brewery.integration.spotify;

import lombok.Builder;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
public class SpotifyGetPlaylistsResponse {
    public SpotifyPlaylist playlists;
}
