package br.com.jorge.brewery.integration.spotify;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

import java.util.List;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@ToString
@EqualsAndHashCode
public class SpotifyPlaylist {
    private String href;
    private List<SpotifyPlaylistItem> items;
}
