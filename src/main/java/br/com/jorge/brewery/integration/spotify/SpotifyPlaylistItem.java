package br.com.jorge.brewery.integration.spotify;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@ToString
@EqualsAndHashCode
public class SpotifyPlaylistItem {
    private String name;
    private SpotifyTracks tracks;
}
