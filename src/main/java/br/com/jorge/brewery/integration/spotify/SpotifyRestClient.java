package br.com.jorge.brewery.integration.spotify;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public interface SpotifyRestClient {
    SpotifyPlaylist getPlaylistByName(String name);
}
