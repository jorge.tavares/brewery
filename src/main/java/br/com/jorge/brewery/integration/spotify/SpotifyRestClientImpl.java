package br.com.jorge.brewery.integration.spotify;

import br.com.jorge.brewery.core.exception.IntegrationException;
import br.com.jorge.brewery.core.properties.IntegrationSpotifyApiProperties;
import br.com.jorge.brewery.core.properties.IntegrationSpotifyApiUrlProperties;
import lombok.AllArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Component
@AllArgsConstructor
class SpotifyRestClientImpl implements SpotifyRestClient {

    private RestTemplate restTemplate;
    private IntegrationSpotifyApiUrlProperties spotifyApiUrlProperties;
    private IntegrationSpotifyApiProperties spotifyApiProperties;

    @Override
    public SpotifyPlaylist getPlaylistByName(String name) {
        final String authorizationToken = getAuthorizationToken();
        String searchPlaylistUrl = spotifyApiUrlProperties.getSearch() + "?q=" + name + "&type=playlist&limit=1&offset=0";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", authorizationToken);
        final HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        try {
            final ResponseEntity<SpotifyGetPlaylistsResponse> response = restTemplate.exchange(searchPlaylistUrl,
                    HttpMethod.GET, httpEntity, SpotifyGetPlaylistsResponse.class);
            final SpotifyGetPlaylistsResponse body = response.getBody();
            return requireNonNull(body).getPlaylists();
        } catch (RestClientException e) {
            throw new IntegrationException("Fail to get the Spotify playlists", e);
        }
    }

    private String getAuthorizationToken() {
        String credential = spotifyApiProperties.getClientId() + ":" + spotifyApiProperties.getClientSecret();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("Authorization", "Basic " + Base64.getEncoder().encodeToString(credential.getBytes()));
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        final HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(requestBody, headers);
        try {
            final ResponseEntity<SpotifyClientCredentialResponse> response = restTemplate.exchange(spotifyApiUrlProperties.getAuthorization(),
                    HttpMethod.POST, httpEntity, SpotifyClientCredentialResponse.class);
            final SpotifyClientCredentialResponse body = response.getBody();
            return requireNonNull(body).getTokenType() + " " + body.getAccessToken();
        } catch (RestClientException e) {
            throw new IntegrationException("Fail to get the Spotify authorization token", e);
        }
    }

}
