package br.com.jorge.brewery.integration.spotify;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@ToString
@EqualsAndHashCode
public class SpotifyTracks {
    private String href;
    private Integer total;
}
