package br.com.jorge.brewery.machine;

import br.com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import br.com.jorge.brewery.integration.spotify.SpotifyRestClient;
import br.com.jorge.brewery.beer.Beer;
import br.com.jorge.brewery.beer.BeerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Service
@AllArgsConstructor
class BreweryMachineImpl implements BreweryMachine {

    private final BeerService beerService;
    private final SpotifyRestClient spotifyRestClient;

    @Override
    public IdealBeer getIdealBeer(Integer temperature) {
        final Beer ideal = beerService.findIdeal(temperature);
        final SpotifyPlaylist playlist = spotifyRestClient.getPlaylistByName(ideal.getStyle());
        return IdealBeer.of(ideal.getStyle(), playlist);
    }
}
