package br.com.jorge.brewery.machine;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
@ToString
@EqualsAndHashCode
public class IdealPlaylist {
    private String self;
    private String name;
    private IdealTracks tracks;
}
