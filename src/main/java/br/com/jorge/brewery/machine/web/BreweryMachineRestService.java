package br.com.jorge.brewery.machine.web;

import br.com.jorge.brewery.machine.BreweryMachine;
import br.com.jorge.brewery.machine.IdealBeer;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static br.com.jorge.brewery.core.SwaggerConstant.*;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@RestController
@AllArgsConstructor
@RequestMapping("/v1/brewery-machines")
class BreweryMachineRestService  {

    private BreweryMachine beerService;

    @PostMapping("/ideals")
    @ApiOperation(nickname = "Get Ideal", value = "Gets a ideal beer by temperature", response = IdealBeer.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = HTTP_200_MSG),
            @ApiResponse(code = 404, message = HTTP_404_MSG),
            @ApiResponse(code = 500, message = HTTP_500_MSG)
    })
    @ResponseStatus(HttpStatus.OK)
    IdealBeer getIdeal(@RequestBody IdealTemperatureRequest request) {
        return beerService.getIdealBeer(request.getTemperature());
    }
}
