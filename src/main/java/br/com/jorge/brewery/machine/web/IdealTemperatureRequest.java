package br.com.jorge.brewery.machine.web;

import lombok.Builder;
import lombok.Value;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@Value
@Builder
class IdealTemperatureRequest {
    public Integer temperature;
}
