package br.com.jorge.brewery.core;

import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractTest {

    public abstract void init();

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates("com.jorge.brewery.template");
    }

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);
        this.init();
    }
}
