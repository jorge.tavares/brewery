package br.com.jorge.brewery.machine;

import br.com.jorge.brewery.integration.spotify.SpotifyRestClient;
import br.com.six2six.fixturefactory.Fixture;
import br.com.jorge.brewery.beer.Beer;
import br.com.jorge.brewery.beer.BeerService;
import br.com.jorge.brewery.core.AbstractTest;
import br.com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;

import static br.com.jorge.brewery.template.FixtureTemplateType.IDEAL;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class BreweryMachineImplTest extends AbstractTest {

    @Mock
    private BeerService beerService;

    @Mock
    private SpotifyRestClient spotifyRestClient;

    private BreweryMachine breweryMachine;

    @Override
    public void init() {
        breweryMachine = new BreweryMachineImpl(beerService, spotifyRestClient);
    }

    @Test
    public void should_get_ideal() {
        Beer beer = Fixture.from(Beer.class).gimme(IDEAL.name());
        SpotifyPlaylist spotifyPlaylist = Fixture.from(SpotifyPlaylist.class).gimme(IDEAL.name());

        when(beerService.findIdeal(-2)).thenReturn(beer);
        when(spotifyRestClient.getPlaylistByName(beer.getStyle())).thenReturn(spotifyPlaylist);

        assertEquals(IdealBeer.of(beer.getStyle(), spotifyPlaylist), breweryMachine.getIdealBeer(-2));

        InOrder inOrder = Mockito.inOrder(beerService, spotifyRestClient);
        inOrder.verify(beerService, times(1)).findIdeal(-2);
        inOrder.verify(spotifyRestClient, times(1)).getPlaylistByName(beer.getStyle());
        inOrder.verifyNoMoreInteractions();
    }
}