package br.com.jorge.brewery.template;

import br.com.jorge.brewery.machine.IdealBeer;
import br.com.jorge.brewery.machine.IdealPlaylist;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class IdealBeerTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(IdealBeer.class).addTemplate(FixtureTemplateType.IDEAL.name(), new Rule() {{
            add("beerStyle", "Dunkel");
            add("playlist", one(IdealPlaylist.class, FixtureTemplateType.IDEAL.name()));
        }});

    }
}
