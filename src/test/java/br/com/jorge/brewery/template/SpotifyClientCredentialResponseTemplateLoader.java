package br.com.jorge.brewery.template;

import br.com.jorge.brewery.integration.spotify.SpotifyClientCredentialResponse;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import static br.com.jorge.brewery.template.FixtureTemplateType.VALID;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyClientCredentialResponseTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyClientCredentialResponse.class).addTemplate(VALID.name(), new Rule() {{
            add("accessToken", "BQBWg65NXKaPM3JsFhgVGe9QJlbMy_FEJaFyN8exVfvNDspG6JUIMw75uazi33AefMmVwNh9ufcWqPMgG7E");
            add("tokenType","Bearer");
            add("expiresIn", 3600);
            add("scope", "");
        }});

    }
}
