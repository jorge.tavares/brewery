package br.com.jorge.brewery.template;

import br.com.jorge.brewery.integration.spotify.SpotifyGetPlaylistsResponse;
import br.com.jorge.brewery.integration.spotify.SpotifyPlaylist;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

import static br.com.jorge.brewery.template.FixtureTemplateType.*;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyGetPlaylistsResponseTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyGetPlaylistsResponse.class).addTemplate(IDEAL.name(), new Rule() {{
            add("playlists", one(SpotifyPlaylist.class, IDEAL.name()));
        }});

    }
}
