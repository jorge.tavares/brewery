package br.com.jorge.brewery.template;

import br.com.jorge.brewery.integration.spotify.SpotifyPlaylistItem;
import br.com.jorge.brewery.integration.spotify.SpotifyTracks;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

/**
 * @author Jorge Tavares jorge.tavares.inatel@gmail.com
 */
public class SpotifyPlaylistItemTemplateLoader implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(SpotifyPlaylistItem.class).addTemplate(FixtureTemplateType.IDEAL.name(), new Rule() {{
            add("name", "matilda dunkel");
            add("tracks", one(SpotifyTracks.class, FixtureTemplateType.IDEAL.name()));
        }});

    }
}
